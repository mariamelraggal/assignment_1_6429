/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fit;

/**
 *
 * @author Yosf
 */
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import static javafx.scene.paint.Color.BLUE;
import static javafx.scene.paint.Color.GREEN;
import javafx.scene.text.Text;

public class Results {
       private static Swimming obj = new Swimming();
     private static Running obj2 = new Running();
       private static KickBoxing obj3 = new KickBoxing();
     private static StrengthTraining obj4 = new StrengthTraining();
     private static int totalcal=0;
     private static double rate=80.0;
     private static double u=80.0;
    private static DecimalFormat df = new DecimalFormat("0.000");
     
    public static void display(String act,int time)
    {   

        Stage window = new Stage();
    window.setTitle("Results");
    Text m=new Text("Activity Recorded Successfully");
    m.setFill(GREEN);
    Label a = new Label(); //for each activity
    Label rr = new Label();
    Label s= new Label(); //sorting swimming
    Label sh=new Label(); 
    Label r=new Label(); //sorting running
    Label rh=new Label();
    Label k=new Label(); //sorting kickboxing
    Label kh=new Label(); 
    Label st=new Label(); //sorting strength training
    Label sth=new Label();
    Text d=new Text("     *Activites Ranking:");
    d.setFill(BLUE);
    GridPane grid = new GridPane();

           switch (act) {
               case "Swimming":
                   
                   obj.totalcal += obj.getburnedcalories(time);
                  totalcal+=obj.getburnedcalories(time);
                   a.setText("  cal burned="+totalcal+" calories");
                   u=rate;
                   obj.Hrate=obj.theartrate(time,rate);
                  rate=obj.Hrate;
                  obj.rateinc=obj.rateincrease(rate,u);
                   rr.setText("  heartrate="+df.format(obj.Hrate)+" beat/minute");
                   break;
               case "Running":
                   
                   obj2.totalcal +=obj2.getburnedcalories(time);
                 totalcal+=obj2.getburnedcalories(time);
                 u=rate;
                 obj2.Hrate=obj2.theartrate(time,rate);
                 rate=obj2.Hrate;
                 obj2.rateinc=obj2.rateincrease(rate,u);
                   a.setText("  cal burned="+totalcal+" calories");
                   rr.setText("  heartrate="+df.format(obj2.Hrate)+" beat/minute");
                   break;
               case "KickBoxing":
                   obj3.totalcal +=obj3.getburnedcalories(time);
                   totalcal+=obj3.getburnedcalories(time);
                   u=rate;
                   obj3.Hrate=obj3.theartrate(time,rate);
                   rate=obj3.Hrate;
                   obj3.rateinc=obj3.rateincrease(rate,u);
                   a.setText("  cal burned="+totalcal+" calories");
                   rr.setText("  heartrate="+df.format(obj3.Hrate)+" beat/minute");
                   break;
               case "StrengthTraining":
                   obj4.totalcal +=obj4.getburnedcalories(time);
                   totalcal+=obj4.getburnedcalories(time);
                   u=rate;
                 obj4.Hrate=obj4.theartrate(time,rate);
                 rate=obj4.Hrate;
                 obj4.rateinc=obj4.rateincrease(rate,u);
                   a.setText("  cal burned="+totalcal+" calories");
                  rr.setText("  heartrate="+df.format(obj4.Hrate)+" beat/minute");
                   break;
           }
           Activity arr[]={obj,obj2,obj3,obj4};
         Arrays.sort(arr, new Comparator<Activity>() {
            @Override
            public int compare(Activity a1, Activity a2) {
                if (a1.totalcal != a2.totalcal) {
                    return a2.totalcal - a1.totalcal;
                }
                return (a2.rateinc > a1.rateinc) ? 1 : -1;
            }
        });
         int x=6;
        
     for (int i=0;i<4;i++)
     { if (arr[i] instanceof Swimming)
     {     s.setText("-total calories burned for swimming="+obj.totalcal+" calories");
           sh.setText("heart rate increase for swimming="+df.format(obj.rateinc)+" beat/minute");
           grid.add(s,1,++x);
           grid.add(sh,1,++x);

     }
     else if(arr[i] instanceof Running)
     { 
         r.setText("-total calories burned for running="+obj2.totalcal+" calories");
         rh.setText("heart rate increase for running="+df.format(obj2.rateinc)+" beat/minute");
         grid.add(r,1,++x);
         grid.add(rh,1, ++x);

     }
     else if(arr[i] instanceof KickBoxing)
     { 
         k.setText("-total calories burned for kickboxing="+obj3.totalcal+" calories");
         kh.setText("heart rate increase for kickboxing="+df.format(obj3.rateinc)+" beat/minute");
         grid.add(k,1,++x);
         grid.add(kh,1,++x);

     }
     else if(arr[i] instanceof StrengthTraining)
     { 
         st.setText("-total calories burned for strengthtraining="+obj4.totalcal+" calories");
         sth.setText("heart rate increase for strengthtraining="+df.format(obj4.rateinc)+" beat/minute");
         grid.add(st,1,++x);
         grid.add(sth,1,++x);
        
     }
     }

    Button btn = new Button();
        btn.setText("add another activity or close program");
                btn.setOnAction(e -> {
             window.close();        
            });
                
   
        grid.add(a,0,1);
        grid.add(m,1,0);
        grid.add(btn,2,17);
        grid.add(rr, 0, 2);
        grid.add(d,0,6);
        Scene scene = new Scene(grid, 800, 250);
       window.setScene(scene);
    window.showAndWait();
    }
}
