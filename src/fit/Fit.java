/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fit;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.BLUE;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Yosf
 */
public class Fit extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Text act = new Text("enter activity:");
        Text time = new Text("enter time in minute:");
        act.setFill(BLUE);
        time.setFill(BLUE);
         Text t =new Text();
        String act1[]={"Swimming","Running","KickBoxing","StrengthTraining"};
        ComboBox<String> c=new ComboBox(FXCollections.observableArrayList(act1));
        TextField time1 = new TextField();
        Button btn = new Button();
       GridPane grid = new GridPane();
        btn.setText("submit");
        btn.setOnAction((ActionEvent event) -> {
            try{
                String x=c.getValue();
                t.setText(" ");
                int y =Integer.parseInt(time1.getText());
                Results.display(x,y);
            } catch(Exception e)
            {
                t.setText("ERROR enter time or Activity please");
                t.setFill(Color.RED);
                
            }
        });
        Button b=new Button();
        b.setText("close program");
                b.setOnAction((ActionEvent event) -> {
                    primaryStage.close();
        });
        
        grid.add(act,0,0);
        grid.add(c,2,0);
        grid.add(time,0,1);
        grid.add(time1,2,1);
        grid.add(btn,1,2);
        grid.add(b,2, 3);
        grid.add(t,0,6);
        Scene scene = new Scene(grid, 500, 200);
        
        primaryStage.setTitle("Fitness Tracker");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
